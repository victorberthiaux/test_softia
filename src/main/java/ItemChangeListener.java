import db.ConventionTable;
import db.StudentTable;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class ItemChangeListener implements ItemListener {

    public String current;
    public JTextField tf;
    public JTextArea ta;
    public StudentTable st;
    public ConventionTable ct;

    public ItemChangeListener(JTextArea ta, JTextField tf, StudentTable st, ConventionTable ct) {
        this.ta = ta;
        this.tf = tf;
        this.ct = ct;
        this.st = st;
    }

    @Override
    public void itemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            Object item = event.getItem();
                this.current = (String) item;
                String[] s = this.current.split(". ");
                String[] s1 = this.current.split(" ");
                int n = st.etudiants_get().get(Integer.parseInt(s[0])).getConvention();
                this.tf.setText(ct.conventions_get().get(n).getName());
                this.ta.setText("Bonjour " +  s1[2] + " " + s1[1] + ",\n\n\n" +
                        "Vous avez suivi " + ct.conventions_get().get(n).getNbHeur() + "h de formation chez FormationPlus.\n\n" +
                        "Pouvez-vous nous retourner ce mail avec la pièce jointe signée.\n\n\n" +
                        "Cordialement,\n\n" +
                        "FormationPlus");
        }
    }
}