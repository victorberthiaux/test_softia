import db.AttestationTable;
import db.ConventionTable;
import db.StudentTable;
import objs.Convention;
import objs.Etudiant;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String [] args) throws IOException {
        AttestationTable at = new AttestationTable();
        ConventionTable ct = new ConventionTable();
        StudentTable st = new StudentTable();
        JFrame frame = new swing2(st, at, ct);
    }
}
