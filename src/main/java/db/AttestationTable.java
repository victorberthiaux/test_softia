package db;

import java.io.*;
import java.util.ArrayList;
import objs.*;

public class AttestationTable {

    private ArrayList<Attestation> attestations;

    public AttestationTable() throws IOException {

        this.attestations = new ArrayList<Attestation>();

        String file ="./src/dbfiles/att.csv";
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String currentLine = reader.readLine();
        currentLine = reader.readLine();
        if (currentLine == null) {
            reader.close();
            return;
        }
        while (currentLine != null) {
            String[] arr = currentLine.split(";");
            Attestation n = new Attestation(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), arr[2]);
            this.attestations.add(n);
            currentLine = reader.readLine();
        }
        reader.close();
    }

    public int add(Attestation e) throws IOException {
        this.attestations.add(e);
        String s = e.etudiant_get() + ";" + e.convention_get() + ";" + e.message_get().replaceAll("\n", "<nl>");
        FileWriter fw = new FileWriter("./src/dbfiles/att.csv", true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(s);
        bw.newLine();
        bw.close();
        return this.attestations.size();
    }

    public ArrayList<Attestation> conventions_get() {
        return this.attestations;
    }
}