package db;

import java.io.*;
import java.util.ArrayList;
import objs.*;

public class ConventionTable {

    private ArrayList<Convention> conventions;

    public ConventionTable() throws IOException {

        this.conventions = new ArrayList<Convention>();

        String file ="./src/dbfiles/conv.csv";
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String currentLine = reader.readLine();
        currentLine = reader.readLine();
        if (currentLine == null) {
            reader.close();
            return;
        }
        while (currentLine != null) {
            String[] arr = currentLine.split(";");
            Convention n = new Convention(arr[0], Integer.parseInt(arr[1]));
            this.conventions.add(n);
            currentLine = reader.readLine();
        }
        reader.close();
    }

    public int add(Convention e) throws IOException {
        this.conventions.add(e);
        String s = e.getName() + ';' + e.getNbHeur();
        FileWriter fw = new FileWriter("./src/dbfiles/conv.csv", true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(s);
        bw.newLine();
        bw.close();
        return this.conventions.size();
    }

    public ArrayList<Convention> conventions_get() {
        return this.conventions;
    }
}