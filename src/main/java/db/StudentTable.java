package db;

import java.io.*;
import java.util.ArrayList;

import objs.*;

public class StudentTable {
    private ArrayList<Etudiant> etudiants;

    public StudentTable() throws IOException {
        this.etudiants = new ArrayList<Etudiant>();
        String file ="./src/dbfiles/etu.csv";
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String currentLine = reader.readLine();
        currentLine = reader.readLine();
        if (currentLine == null) {
            reader.close();
            return;
        }
        while (currentLine != null) {
            String[] arr = currentLine.split(";");
            Etudiant n = new Etudiant(arr[0], arr[1], arr[2], Integer.parseInt(arr[3]));
            this.etudiants.add(n);
            currentLine = reader.readLine();
        }
        reader.close();
    }

    public int add(Etudiant e) throws IOException {
        this.etudiants.add(e);
        String s = e.getName() + ";" + e.getSurname() + ";" + e.getEmail() + ";" + Integer.toString(e.getConvention());
        FileWriter fw = new FileWriter("./src/dbfiles/etu.csv", true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(s);
        bw.newLine();
        bw.close();
        return this.etudiants.size();
    }

    public ArrayList<Etudiant> etudiants_get() {
        return this.etudiants;
    }
}
