package objs;
import objs.*;

public class Attestation {
    private int etudiantid;
    private int conventionid;
    private String message;

    public Attestation(int etudiant, int convention, String message) {
        this.etudiantid = etudiant;
        this.conventionid = convention;
        this.message = message;
        this.message = this.message.replaceAll("<nl>", "\n");
    }

    public int etudiant_get() {
        return this.etudiantid;
    }

    public int convention_get() {
        return this.conventionid;
    }

    public String message_get() {
        return this.message;
    }
}
