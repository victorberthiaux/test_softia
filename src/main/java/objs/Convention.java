package objs;

public class Convention {
    private String name;
    private int nbHeur;

    public Convention(String name, int nbHeur) {
        this.name = name;
        this.nbHeur = nbHeur;
    }

    public String getName() {
        return name;
    }

    public int getNbHeur() {
        return nbHeur;
    }
}
