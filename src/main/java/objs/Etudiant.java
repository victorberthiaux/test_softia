package objs;

public class Etudiant {
    private String name;
    private String surname;
    private String email;
    private int conventionid;

    public Etudiant(String name, String surname, String email, int convention) {
        this.conventionid = convention;
        this.email = email;
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getSurname() {
        return surname;
    }

    public int getConvention() {
        return conventionid;
    }
}
