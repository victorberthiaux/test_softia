import db.AttestationTable;
import db.ConventionTable;
import db.StudentTable;
import objs.Attestation;
import objs.Convention;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class swing2 extends JFrame implements ActionListener {

    public JComboBox student(StudentTable st, ItemChangeListener l) {
        String[] los = new String[st.etudiants_get().size()];
        for (int i = 0; i < st.etudiants_get().size(); i++)
            los[i] = i + ". " + st.etudiants_get().get(i).getName() + ' ' + st.etudiants_get().get(i).getSurname();
        JComboBox stlist = new JComboBox(los);
        stlist.setBounds(30, 60, 150, 45);
        stlist.addItemListener(l);
        stlist.setBackground(Color.WHITE);
        stlist.setOpaque(true);
        return stlist;
    }

    public JTextField convention() {
        JTextField convname = new JTextField();
        convname.setBounds(200, 60, 200, 45);
        convname.setEditable(false);
        return convname;
    }

    public JTextArea attestation() {
        JTextArea text = new JTextArea();
        text.setBounds(30, 150, 400, 300);
        return text;
    }

    public JButton bouton() {
        JButton button = new JButton("Create");
        button.setBackground(Color.CYAN);
        button.setBounds(400, 360, 80, 20);
        return button;
    }

    public swing2(StudentTable st, AttestationTable at, ConventionTable ct) {

        super("formulaire");

        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        };
        addWindowListener(l);

        JFrame frame = new JFrame("Form");
        JTextField convname = convention();
        JTextArea text = attestation();
        JButton button = bouton();
        ItemChangeListener ls = new ItemChangeListener(text, convname, st, ct);
        JComboBox stlist = student(st, ls);
        button.addActionListener(new ActionListener() {

                                     @Override
                                     public void actionPerformed(ActionEvent e) {
                                         int et = Integer.parseInt(ls.current.split(". ")[0]);
                                         try {
                                             at.add(new Attestation(et, st.etudiants_get().get(et).getConvention(), text.getText()));
                                         } catch (IOException ioException) {
                                             ioException.printStackTrace();
                                         }
                                     }
                                 });
        frame.add(stlist);
        frame.add(text);
        frame.add(convname);
        frame.add(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Color.darkGray);
        frame.setSize(500,450);
        frame.setLayout(null);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}